import names
from datetime import datetime
import random, time
import numpy as np

def get_name():
	return names.get_full_name()

def get_birth_time():
	sday, eday = datetime(2007, 9, 1), datetime(2010, 9, 1)
	stime, etime = sday.timestamp(), eday.timestamp()
	stamp = random.random()*(etime-stime) + stime	
	dt1 = datetime.fromtimestamp(stamp)
	fmt = "%H:%M:%S %m/%d/%Y"
	sdt1 = dt1.strftime(fmt)
	return sdt1

def get_ethnicity():
	group = ['Asian', 'Black', 'Hispanic', 'White']
	ethnicity = random.choice(group)
	return ethnicity

def get_gender():
	group = ['Female', 'Male']
	gender = random.choice(group)
	return gender

def get_team():
	teams = ['T', 'G', 'M', 'S']
	tm = random.choice(teams)
	return tm

def get_grade():
	grades = ['6', '7', '8']
	grd = random.choice(grades)
	return grd

def get_test_scores(N): #N is total kid number
	mean = [75, 80, 85]
	cov = [[25, 15, 6], [15, 20, 6], [6, 6, 10]] 
	math, eng, pe = np.random.multivariate_normal(mean, cov, N).T
	for i in range(N):
		yield (int(math[i]), int(eng[i]), int(pe[i]))

def build_line(score_generator):
	rec = {}
	rec['name'] = get_name()
	rec['grade'] = get_grade()
	rec['team'] = get_team()
	rec['birth'] = get_birth_time()
	rec['ethnicity'] = get_ethnicity()
	rec['gender'] = get_gender()
	scores = next(score_generator)
	rec['math'] = str(scores[0])
	rec['english'] = str(scores[1])
	rec['PE'] = str(scores[2])
	readings = list(rec.values())
	line = ','.join(readings)
	fields = ','.join(rec.keys())
	return (line, fields)

def generate_file_content(N):
	score_generator = get_test_scores(N)
	lines = ''
	for i in range(N):
		line, fields = build_line(score_generator)
		if i == 0:
			lines = fields + '\n' + line + '\n'
		else:
			lines = lines + line + '\n'
	return lines

N = 1500
fname = 'school-sheet.csv'
seed = random.randint(1, 1000) #946
print(f"the seed is {seed}")
np.random.seed(seed)
random.seed(seed)

tstart = time.time()
lines = generate_file_content(N)
with open(fname, 'w') as handler:
	handler.write(lines)
tend = time.time()
print(f"it takes {tend - tstart} seconds to genearte the file...")