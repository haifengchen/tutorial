la = [3, 6, 7, 9, 11, 4]
# try 1 
# x = la[8]

#try 2
# try:
# 	x = la[8]
# except:
# 	print("the index is out of bound")
# print(x)


#try 2
try:
	x = la[8]
	print(x)
except:
	print("the index is out of bound")


# Explain try/except/Finanlly