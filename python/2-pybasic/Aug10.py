record = {}
record['name'] = 'Helen Chen'
record['math'] = 97


recorda = {'name': 'Helen Chen', 
	   'Math': 97}

 # step 1: read the file (readline)
 # step 2: change each line into a record 
 # step 3: go over the list of record, find the max score for math
 # step 4: go over the list of record, find the max score for language
 # step 5: go over the list of record, find the max score for PE


s1 = 'Helen'
s2 = ' H elen '
s3 = s2.strip()
print(s3)

line = 'Claire Tang,99,98,87'
la = line.split(',')
print(la)
math_score = int(la[1])
record = {'name': la[0], 
	  'math': math_score}

records = []
records.append(record)

dt = '8-10-2021'
lb = dt.split('-')

lc = ['Claire', 'Helen', 'Nancy']
s4 = ','.join(lc)
print(s4)

#---- import ----
# $ pip3 install matplotlib

import matplotlib.pyplot as plt
numbers = [2, 5, 4, 7, 1, 9]
plt.plot(numbers)
plt.show()

x = range(-50, 50)
y = [i**2 for i in x]
plt.plot(x, y)
plt.xlabel('domain')
plt.ylabel('square(x)')
plt.show()

scores = [92, 87, 79, 99]
labels = ['Math', 'Art', 'PE', 'Scirence']
plt.pie(scores, labels= labels)
plt.show()

labels = ['60-70', '70-80', '80-90', '90-100']
numbers = [150, 200, 350, 100]
plt.pie(numbers, labels= labels)
plt.show()
