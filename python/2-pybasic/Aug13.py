# swap
la = [3, 2, 5, 7]
tmp = la[2]
la[2] = la[3]
la[3] = tmp

'Helen' + 'Chen'
[3, 5]*10
s1 = ' AB C\n'
s1.strip()


def find_min_a(la):
	min = la[0]
	imin = 0
	for ix, x in enumerate(la):
		if x < min:
			min = x
			imin = ix
	return (imin, min)


def find_min_b(la):
	for ix in range(len(la)-1):
		if la[ix] < la[ix+1]:
			tmp = la[ix]
			la[ix] = la[ix+1]
			la[ix+1] = tmp
	return la[-1]


la = [3, 5, 1, 8, 9, 6]
imin_a, min_a = find_min_a(la)
min_b = find_min_b(la)
print(imin_a, min_a, min_b)

#--------------------
la = [3, 5, 1, 8, 9, 6]
def sort_a(la):
	lb = []
	while len(la) > 0:
		imin, min = find_min_a(la)
		lb.append(min)
		del la[imin]
	return lb
lb = sort(la)



#--------------------------------------
def exchange(a,b):
	tmp = a
	a = b
	b = tmp
	return (a, b)

la = [3, 5, 1, 8, 9, 6]
def bubble_sort(la):
	swapped = True
	while swapped:
		swapped = False
		for i in range(len(la)-1):
			if la[i] > la[i+1]:
				la[i], la[i+1] = exchange(la[i], la[i+1])
				swapped = True
	return la
lb = bubble_sort(la)
print(lb)

#--------
courses = ['math', 'art', 'science', 'PE']
scores = [98, 89, 92, 96]
import matplotlib.pyplot as plt

pos = list(range(4))
plt.barh(pos, scores, color='blue')
plt.yticks(pos, labels=courses)
plt.show()