
def find_max_a(la):
	mx = la[0]
	mx_index = 0
	for index, x in enumerate(la):
		if x > mx: 
			mx = x
			mx_index = index
	return (mx_index, mx)

def find_max_b(la):
# bubble algorithm
	for i in range(len(la)-1):
		if la[i] > la[i+1]:
			tmp = la[i]
			la[i] = la[i+1]
			la[i+1] = tmp
	return la[-1]

def find_min_a(la):
	min = la[0]
	imin = 0
	for ix, x in enumerate(la):
		if x < min:
			min = x
			imin = ix
	return (imin, min)


def find_min_b(la):
#bubble
	for ix in range(len(la)-1):
		if la[ix] < la[ix+1]:
			tmp = la[ix]
			la[ix] = la[ix+1]
			la[ix+1] = tmp
	return la[-1]

def sort_a(la):
	lb = []
	while len(la) > 0:
		imin, min = find_min_a(la)
		lb.append(min)
		del la[imin]
	return lb

def sort_b(la):
#bubble sort
	swapped = True
	while swapped:
		swapped = False
		for i in range(len(la)-1):
			if la[i] > la[i+1]:
				la[i], la[i+1] = la[i+1], la[i]
				swapped = True
	return la

#-------------- matplotlib --------------

import matplotlib.pyplot as plt

def curve_plot():
	x = range(-50, 50)
	y = [i**2 for i in x]
	plt.plot(x, y) 
	plt.ylabel('x square')
	plt.show()

def pie_plot():
	scores = [92, 87, 97, 79]
	labels = ['Math', 'Art', 'Science', 'PE']
	plt.pie(scores, labels=labels)
	plt.show()

def vertical_bar_plot():
	courses = ['math', 'art', 'science', 'PE']
	scores = [98, 89, 92, 96]
	pos = list(range(4))
	plt.bar(pos, scores, color='blue')
	plt.xticks(pos, labels=courses)
	plt.show()

def horizontal_bar_plot():
	courses = ['math', 'art', 'science', 'PE']
	scores = [98, 89, 92, 96]
	pos = list(range(4))
	plt.barh(pos, scores, color='blue')
	plt.yticks(pos, labels=courses)
	plt.show()