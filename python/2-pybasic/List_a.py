# sqaure bracket with comma-separated items
la = [1, 2, 3]
lb = ['a', '2']
lc = ['a', 2]

# type conversion, list can only converted from string
ia = int(3.2)
la = list('Helen')

#define an empty list, used for future
la = []
ls = list()

#list method
la = list('abccd')
len(la)
la.count('c'), la.count('c')
#index, start from 0, keep the order in the list
la = list('Clair')
la.count('i')
la.index('C'), la.index('c'), la.index('i') 
la[0], la[2]



la = [1, 3, 5, 6]
la.count(3)

la.reverse() #in-place 
lb = la.reverse() #wrong

la = list('Helen'), 
lb = ['Helen'], 
lc = ['Helen', 'Clair']
la.append('f')
lb = list('Clair')
la.insert(0, 'x') #before 0 index
la.insert(2, 'x')

la.extend(lb)
lc = la + lb
la += lb

#access list element, list can contain other element
la = [7, 3, 4, 6, 8, 10, 12] #value, vs index
la[0], la[2], la[-1], la[-3]

la.pop()
la
la.remove(8) # 1st value, not index
del la[2]
la.clear()

la.sort() #in-place
lb = sorted(la)

#slicing
la[0:1], la[0:2], la[2:5]
la[:3], la[3:], 
la[:-1], la[-3:]

lc = la.copy()

#--------------------------
# Bob has a party, keep the order of who join the party
guests = []
guests.append('Helen')
guests.adppen('Alison')
guests.append('Mike')
guests.append('Richal')
guests, len(guests)
['Clair', 'Emily']
#guests.append(['Clair', 'Emily']) 
guests.extend(['Clair', 'Emily'])
guests[0], guests[3], guests[:3], guests[-3:]
guests.append('Emily')
guests.count('Emily')
guests.insert(0, 'Kio')
guests.insert(2, 'Blair')
guests.remove('Richal')
guests
guest.pop()
guests += ['Alison', 'Clair']
guests.count('Clair')
guests.remove('Clair')
del guests[3]

# value vs address, value copy for int/float/string and other basic types
# for list, disctionary, address copy, not value copy, data change all change
la = ['we','you', 'she']
lb = la
la[0] = 'he'
#lb will change as well!!! data, link
lb = la.copy()
la[0] = 'XXX'

fa = 1.2
fb = fa
fa = 1.3

sa = 'wwea'
sb = sa
sa = 'wwwwww'
