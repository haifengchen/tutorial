is_sleeping = True
time = 1
while is_sleeping:
	print('dons call me')
	time += 1
	if time == 10 :
		is_sleeping = False

#--------
is_sleeping = True
time = 1
while time < 10:
	print('dont call me')
	time += 1

#-- 
count = 0
while True:
	print("running")
	count += 1
	if count == 10:
		break

kids = ['Helen', 'Clair', 'Bob', 'Nancy', 'Mike']
for index, kid in enumerate(kids):
	if kid == 'Nancy':
		print(f'index of Nancy is {index}')
		break

kids = {'Helen':'F', 'Clair': 'F', 'Bob': 'M', 'Nancy': 'F', 'Mike':'M'}
for kid in kids:
	if kids[kid] == 'F':
		print(kid)

for kid in kids:
	if kids[kid] == 'M':
		continue
	print(kid)


la = [3, 4, 8]
lb = [2*x for x in la]

lb = []
for i in range(101):
	if i % 2 == 0:
		lb.append(i)
print(lb)

lb = [i for i in range(101) if i % 2 == 0]
lc = [i*2 for i in range(51)]

mat = [[9, 8, 7], [2, 4, 6], [8, 2, 5]]
m2 = []
for row in mat: #row = [9, 8,7]
	row2 = [i*2 for i in row]
	m2.append(row2)
print(m2)	
	
m2 = [[i*2 for i in row] for row in mat]
m3 = [i*2 for i in row for row in mat]

#
kids = ['Helen', 'Claire', 'Bob', 'Mike']
sports = ['swimming', 'volleball', 'tennis', 'fencing']

for kid in kids:
	for sport in sports:
		print(f'{kid} is enjoying {sport}')

la = [ f'{kid} is enjoying {sport}' for kid in kids for sport in sports]

print(la)











