#prepare text file, pdf file, image file, word file, execel file
# text file vs binary fiat e
	# cat command introduce
# different mode: r, w, a, +, 'b'
# file_handler, open and close
# try/execpt/finally vs. with
# line by line read vs. whole content read


#open must be paired with close
def f1(fname): 
	handler = open(fname, mode='r')
	lines = handler.readlines() 
	print(lines) # \n is a return character, by system
	handler.close()

# fname = 'myfiles/note_text'
# f1(fname)

def f2(fname): 
	try: 
		handler = open(fname, mode='r')
		lines = handler.readlines() 
		print(lines) # \n is a return character, by system
	except:
		print('something is wrong...')
	finally:	
		handler.close()

# fname = 'myfiles/note_text_2'
# f2(fname)

def f3(fname):
	with open(fname, 'r') as handler:
		lines = handler.readlines()
		for line in lines:
			print(line)
	
# fname = 'myfiles/note_text_2'
# f3(fname)


def f4(fname):
	with open(fname, 'r') as handler:
		for line in handler:
			print(line)
	
# fname = 'myfiles/note_text_2'
# f4(fname)

def f5(fname):
	with open(fname, 'r') as handler:
		content = handler.read()
		print(content)
		print(type(content))
	
fname = 'myfiles/note_text_2'
# f5(fname)

def f6(fname):
	with open(fname, 'rb') as handler:
		content = handler.read()
		print(content)
		print(type(content))
	
fname = 'myfiles/image.JPG'
# f6(fname)

#---------- wite fils -------
def f7(fname):
	with open(fname, 'w') as handler:
		handler.write('hello, Good Thursday')
	
fname = 'myfiles/newfile'
# f7(fname)

#also accept list for write
def f8(fname):
	la = ['Helen', 'Claire', 'Bob']
	la = ['Helen\n', 'Claire\n', 'Bob']
	with open(fname, 'w') as handler:
		handler.writelines(la)
	
fname = 'myfiles/newfile'
# f8(fname)

#write will overwrite a file, if the file exists
def f9(fname):
	la = ['Mike\n', 'Nancy\n', 'Alison']
	# with open(fname, 'w') as handler:
	with open(fname, 'a') as handler:
		handler.writelines(la)
	
fname = 'myfiles/newfile'
# f9(fname)

