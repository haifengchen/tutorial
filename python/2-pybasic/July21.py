la = [3, 5, 11, 2, 8, 4]
mx = la[0]
for i in la:
	if i > mx: 
		mx = i
print(f'the max is {mx}')

la = [3, 5, 11, 2, 8, 4]
sm = 0
for i in la:
	sm += i
print(f'the sum is {sm}')

la = [3, 5, 11, 2, 8, 4]
#lb = [6, 10. 22, 4, 16, 8]
lb = []
for i in la:
	lb.append(i*2)
print(f'the new list is {lb}')

kids = ['Helen', 'Claie', 'Nancy', 'Alison']
sports = ['swimming', 'fencing', 'skating', 'tennis', 'xc']

for kid in kids:
	for sport in sports:
		print(f'{kid} is enjoying {sport}')
