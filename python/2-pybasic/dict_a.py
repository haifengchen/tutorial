#key, value pairs, 
#map key to values, 
helen = {'first_name': 'Helen',
         'last_name': 'Chen',
	 'age': 12,
	 'school': 'GMS',
	 'hobby': 'annimate'}

claire = {'first_name': 'Claire',
         'last_name': 'Tang',
	 'age': 13,
	 'school': 'GMS',
	 'hobby': 'volleball'}

numbers = dict(one=1,two=2,three=3)

helen_list =[('first_name','Helen'),
	      ('last_name','Chen'),
	     ('email','melon@gmail.com')]
helen_dict = disct(helen_list)

#access the dictionary
helen['first_name']
helen['age']
'address' in helen
'hobby' in helen
#compare with List, index vs. key
la = ['Helen', 'Claire', 'Emily']
la[0]

'address' not in helen
'hobby' not in helen

helen.get('age')
helen.get('age', 11)
helen.get('address', 'NOt found')
helen.clear()

#------------------------------------
# value vs address, value copy for int/float/string and other basic types
# for list, disctionary, address copy, not value copy, data change all change
la = ['we','you', 'she']
lb = la
la[0] = 'he'
#lb will change as well!!! data, link
lb = la.copy()
la[0] = 'XXX'

fa = 1.2
fb = fa
fa = 1.3

sa = 'wwea'
sb = sa
sa = 'wwwwww'
#------------------------------------

helen_1 = helen
helen_2 = helen.copy()
#compare Helen and Helen_1, exactly same, change together
helen['hobby'] = 'swimming'

helen.items() # a list of dictionary items
keys = helen.keys()
'age' in helen.keys()
values = helen.values()
'Chen' in values

#remove a key, value
helen.pop('age') #key
helen.popitem() #last item
helen.update([('team', 'PTAC')])
helen.update([('team', 'PTAC'), ('GPA', 5)])

helen.update([('age', 13)])
helen['age'] = 13
helen['loactaion'] = 'NJ'

del helen['location']
helen.pop('location')







