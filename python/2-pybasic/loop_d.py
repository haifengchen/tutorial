
#String for loop
name = 'Helen'
for letter in name:
	print(letter)

letters = [l for l in name]
letter_dict = {key:value for key,value in enumerate('Helen')}

#find all the prime numbers below 100
for num in range(2, 100):
	i = 1
	while i <= num:
		i += 1
		if num % i ==0: 
			break
	if i == num: 
		print(num)

