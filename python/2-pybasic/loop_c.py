#A while loop is created with the keyword while followed by an expression. 
# In other words, while loops will run until a specific condition fails, or is no longer truthy.
count = 0
while count < 10 :
	print(count)
	count += 1

while True:
	print('running')

runnable = True
count = 0
while runnable:
	print('running')
	count += 1
	if count == 10 :
		runnable = False

#Breaking Out of a Loop
count = 0
while True:
	print('running')
	count += 1
	if count == 10 :
		break

la = ['Helen', 'Claire', 'Nancy', 'Bob', 'Mike']
for pos, name in enumerate(la):
	if name == 'Nancy':
		print(f'position is {pos}')
		break

gender_dict = {'Helen': 'F', 'Claire': 'F', 'Nancy': 'F', 'Bob': 'M', 'Mike': 'M'}
for name in gender_dict:
	if gender_dict[name] == 'M':
		print(name)

for name in gender_dict:
	if gender_dict[name] == 'F':
		continue
	print(name)

#every number *2 
la = [1, 3, 5, 6]
lb = []
for i in la:
	lb.append(i*2)

lb = [i*2 for i in la]

#filter numbers
lb = []
for i in range(10):
	if i %2 ==0: 
		lb.append(i)

lb = [i for i in range(10) if i % 2 ==0]

#add 2 list numbers
kids = ['Helen', 'Claire', 'Nancy', 'Alison']
activities = ['swim', 'fence', 'skating', 'tennis']
output = []
for kid in kids:
	for activity in activities: 
		status = f'{kid} is enjoying {activity}'
		output.append(status)

output = [f'{kid} is enjoying {activity}' for kid in kids for activity in activities]


#matrix value *2, NESTED
matrix=[[9,8,7],[6,5,4],[3,2,1]]
m2 = [[element*2 for element in row] for row in matrix]
