
def print_factors(a):
	for i in range(1, a+1):
		if a % i == 0:
			print(i)

print_factors(10)
print_factors(16)



def print_factors_b(a):
	la = []
	for i in range(1, a+1):
		if a % i == 0:
			la.append(i)
	print(la)
	return la

lb = print_factors_b(10)
print(lb)

def welcome(school, student):
	print(f'{school} welcome {student}')

welcome('GMS', 'Claire')
welcome('CMS', 'Alison')

def myfun(para):
	a = 12
	b = para + a
	return b

c = myfun(10)
print(c)

#arguments
def welcome(student, school='GMS'):
	print(f'{school} welcome {student}')

welcome('Claire')
welcome('Alison', 'CMS')

#----------------
#arguments
def find_max(*args):
	#args = [10, 12, 3, 7, 8]
	mx = args[0]
	print(len(args))
	for i in args:
		if i > mx:
			mx = i
	return mx


mx = find_max(1, 2, 3)
mx = find_max(10, 12, 3, 7, 8)
mx = find_max(10, 12, 3, 7, 8, 9, 23)

#----------
def find_max(name, *args):
	#args = [10, 12, 3, 7, 8]
	mx = args[0]
	print(len(args))
	for i in args:
		if i > mx:
			mx = i
	print(f"student {name} has max score {mx}")		
	return mx

mx = find_max('Helen', 97, 95, 92, 89)
mx = find_max('Claire', 98, 92, 96)

