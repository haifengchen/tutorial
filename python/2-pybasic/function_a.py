# function -- paragraph, but reusable
def print_factors():
	a = 12
	for i in range(1, a+1):
		if a % i == 0:
			print(i)

#function won't run, only delared, need to call 
print_factors()

#parameter make function reusable
#argument
def print_factors(a):
	for i in range(1, a+1):
		if a % i == 0:
			print(i)

print_factors(12)
print_factors(22)

def welcome(name):
	print(f'Welcome {name}')

welcome('Helen')
welcome('Claire')

#multiple parameters
def print_prime_numbers(low, high):
	for num in range(low, high):
		i = 1
		while i <= num:
			i += 1
			if num % i ==0: 
				break
		if i == num: 
			print(num)

# order matters, 1st is low, 2nd is high (position argument)
print_prime_numbers(1, 30)
print_prime_numbers(1, 100)
# switch, keyword argument
print_prime_numbers(high=100, low=1)
#functions to only accept a small number of arguments, keyword arguments or both.

# variable scope
def myfun(para):  #para is virtual
	a = 12
	b = para + a
	print(b)

w = 23 #can be different
print(a)
print(b)
print(myfun(w))

#default argument
def welcome(namex,age=15):
	print(f'Welcome {name}. You are {age} years old.')
welcome('Helen', 12)
welcome('Bob')

#*args - An arbitrary number of arguments
# accept any # of argument
# example: python max(a, b, c) 
max(6, 2, 8)
max(6, 2, 8, 12, 6)
def my_max(*args):
	print(args)
	print('the type of args is:', type(args))
	print(len(args))
	print(args[0])
	print(args[-1])
my_max(2, 4, 1, 8, 9)
#input is many paras, in function become a Tuple
my_max((2, 4, 1, 8, 9))
# * is for deconstruction, previous is for any number
my_max(*(2, 4, 1, 8, 9))

my_max([2, 4, 1, 8, 9])
# * is for deconstruction, previous is for any number
my_max(*[2, 4, 1, 8, 9])

def myfun(a, *args):
	print(f'a is {a}')
	print(f'args is {args}')
myfun(12, 3, 5, 8)

#------adding keyword arguments--------------

def welcome(student, school='GMS'):
	print(f'{school} welcome {student}')

welcome('Claire')
welcome('Alison', 'CMS')

def welcome(student, school):
	print(f'{school} welcome {student}')
welcome('Helen', school='GMS')
#welcome(student='Claire', 'CMS')

def summation(student, math, art, science):
	sum = math + art + science
	print(f'{student} has score {sum}')

summation('Helen', 88, 92, 97)
summation('Helen', math=88, art=92, science=97) #easy to read

#**kargs
def myfun2(**kwargs):
	print(kwargs)
	print('the type of args is:', type(kwargs))
	print(len(kwargs))
	print(kwargs.keys())
	print(kwargs.values())
	print(kwargs['name'])
	print(kwargs.get('age', 13))
	
myfun2(name='Helen', age=12, school='GMS')
myfun2(name='Helen', school='GMS')
#wrong
# myfun2('Helen', 12, 'GMS')
# myfun2('Helen', 12, school='GMS')

def myfun3(district, **kwargs):
	print(kwargs)
	print('the type of args is:', type(kwargs))
	print(len(kwargs))
	print(kwargs.keys())
	print(kwargs.values())
	print(kwargs['name'])
	print(kwargs.get('age', 13))
	
myfun3('WWP', name='Helen', age=12, school='GMS')
myfun3(district='WWP', name='Helen', age=12, school='GMS')
#----------------

def myfunc4(*args, **kwargs):
	print(args)
	print(kwargs)

myfunc4(23, 'WWP', name='Claire', school='GMS')

def myfunc5(state, *args, **kwargs):
	print(args)
	print(kwargs)
#check the changes
myfunc5(23, 'WWP', name='Claire', school='GMS')
myfunc5('NJ', 23, 'WWP', name='Claire', school='GMS')
