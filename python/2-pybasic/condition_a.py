a = 2.3
a > 3
a = 3
a == 3
a <= 3
a != 3

name = 'Helen'
name == 'Claire'
name != 'Nancy'
name == 'Helen'

allowed = name == 'Helen'
allowed = name in ('Helen', 'Claire', 'Nancy')

temp = 92
too_high = temp > 90

#------------------------
#if<expression>:
#   do something here
#------------------------
#indent is important
# Python cares about indentation. A code block is a series of lines of code that is indented uniformly. Python determines where a code block begins and ends by this indentation.
# Other languages use parentheses or semi-colons to mark the beginning or end of a code block
air = 'off'
temp = 92
too_high = temp > 90
if too_high : 
	air = 'on'
if temp > 90:
	air = 'on'

if temp > 90:
	air = 'on'
else: 
	air = 'off'

go_out = False
if temp>=50 and temp <70:
	go_out = True

#code block
if temp > 90:
	door = 'close'
	window = 'close'
	air = 'on'
	temp -= 3
	print('turn on air, temprature goes down')
else:
	air = 'off'
	window = 'open'
	print('turn off air')

#nested if 

age = 14
school = 'GMS'
if age >= 12 and age <= 16 :
	if school in ['GMS', 'CMS']:
		print('you are in WWP')
	elif school in ['PUMS', 'MongomeryMS']:
		print('you are in Princeton area')
else:
	print('you are not middle school student')


#-----------
name = 'Helen'
print(name)
print('name')
print('Helen')
print(Helen)

print('name is '+ name)
print('name is name')
print(f'name is {name}')
