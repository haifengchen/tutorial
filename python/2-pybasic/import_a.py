import math
dir(math)
math.sqrt(4)
math.log(100)
math.log(8)
math.log10(100)
a = math.pi
math.sin(math.pi/2)
math.cos(math.pi/4)

import math as m
m.log(8)
m.sin(m.pi/2)

from math import log, log10
log(8)
log10(1000)

#namespace, void name confliction
from math import *
log(8)
log10(1000)


from datetime import date
d1 = date(2002, 12, 31)
d2 = date.today()

from datetime import datetime
dt1 = datetime.now()
dt1.month
dt1.day
dt1.hour
dt1.minute