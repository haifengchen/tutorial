la = [3, 6, 8]
for i in la: 
	print(i)

la = [1, 2, 3, 4, 5, 6, 7]
for x in la:
	if x % 2 == 0:
		print(x)

list(range(8))
list(range(2, 8))
list(range(1, 101))
list(range(1, 101, 2))

for x in range(100, 201, 2):
	if x < 168:
		print(x)

GPAs = {'Helen': 4.3, 'Claire': 4.3, 'Nancy': 4.1, 'Bob': 3.5}
GPAs.keys()
GPAs.values()
GPAs.items()

for x in GPAs.keys():
	print(x)

for x in GPAs:
	print(x)

for x in GPAs.values():
	print(x)

for x in GPAs.items():
	print(x)

for k, v in GPAs.items():
	print(k, v)

la = ['Claire', 'Nancy', 'Helen']
for index, name in enumerate(la):
	print(f'the index of {name} is {index}')

#
a = 12348
for i in range(1, a+1):
	if a % i == 0:
		print(i)

