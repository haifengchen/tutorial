ta = 2,3
ta = (2,3)
tb = ('Helen', 'Claire')
tc = tuple('1', '2', '3', '1')

type(ta)

#immuntable, no append/insert/pop/remove/del
len(ta)
tc.count('1')
ta[0]
tb.index('Claire')

#tuple can be joined, but becomes a new couple
ta = (3,4,5)
tb = (7,8 )
tc = ta + tb

ta = tuple()

#list of tuple, immutable vs muttable
kids = [('Helen', 'Chen', 12), 
        ('Claire', 'Tang', 13),
	('Alison', 'Lee', 13),]
