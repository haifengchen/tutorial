#just True and False. but is important, condition check

a = 1
a == 1
b == a
b = 1
b == a # add

True == 1
False == 0
False == True

True = 1

#Python has the concept of “truthy” and “falsey”.
#Anything not zero should be cast as True. for numbers
bool(1)
bool(12)
bool(0)
bool(-1)

# non-Numeric types, True will map to sequences with one or more items and False will map to sequences with zero items.
bool('abs')
bool('')
bool('1')
bool('0')

bool(['a', 'b'])
bool([])
bool({1: 'One'})
bool({})


#Python also has the concept of None, which is Python’s null value. 
# None is a keyword in Python and its data type is NoneType. 
# None is not the same as 0, False or an empty string. 
# In fact, comparing None to anything other than itself will return False:
None == 1
None == []
None == ''
None == None

x = None
