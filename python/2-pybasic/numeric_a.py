print('this is a numeric session')

# integers
ia  = 10
fa = 10.0
sb = '10'  # python knows behind
type(ia), type(sb)
ic = int('10')
# binary number 
id = int('10', 2)  #need math

#float
fa = 2.0
fb = '2.0'
type(fa), type(fb)
fc = float('2.0')

#complex numbers
ca = 1 + 2j
type(ca)
ca.real, ca.imag
cb = complex(10, 12)
cb.conjugate()

a = 13
b = 4
a + b, a - b, a * b, a/6, a//b, a%b
divmod(a, b)
-a, +a, abs(a), 
float(a), int(1.2)
pow(3,2), pow(2,3), 3**2, 2**3

# augmented assignment operators only in CS
a = 2
a = a + 3 # a storage in a memory
a += 3 # += is an operator
a -= 1
a *= 2
a **= 2
a /= 2
a %= 3

#--- exercise --
8 ** (1/3)
b = 13
b %= 3
type(3), type(3.0)
int(3.8) 
a = 2 + 3j
b = 3 +3j
a +b, a-b, a*b, a/b, a.conjugate()

a = '1'
a 

F = 98.6
(F - 32) * (5/9)

#F = ax^2 + bx +c
a = 1, b = -5, c = 6
D =  (b**2 - 4*a*c)**0.5
x1 = (-b + D)/(2*a)
x2 = (-b - D)/(2*a)


#----
a = input()  #user input sth


#--- the above is the building block, individual is like advanced calculator
# but put together, 







