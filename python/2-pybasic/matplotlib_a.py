#python -m pip install matplotlib

import matplotlib.pyplot as plt

numbers = [2, 5, 3, 7, 1, 9]
plt.plot(numbers) 
plt.ylabel('Random numbers') 
plt.show()

x = range(-50, 50)
y = [i**2 for i in x]
plt.plot(x, y) 
plt.ylabel('x square')
plt.show()

scores = [92, 87, 97, 79]
pos = list(range(4))
labels = ['Math', 'Art', 'Science', 'PE']
plt.bar(pos, scores, color='blue')
plt.xticks(pos, labels)
plt.show()

scores = [92, 87, 97, 79]
pos = list(range(4))
labels = ['Math', 'Art', 'Science', 'PE']
plt.barh(pos, scores, color='blue')
plt.yticks(pos, labels)
plt.show()

scores = [92, 87, 97, 79]
labels = ['Math', 'Art', 'Science', 'PE']
# fig1, ax1 = plt.subplots()
# ax1.pie(scores, labels=labels)
plt.pie(scores, labels=labels)
plt.show()