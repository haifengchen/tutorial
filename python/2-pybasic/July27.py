filename = 'note_text'
handler = open(filename, mode='r')
content = handler.read()
print(content)
handler.close()

filename = 'note_text'
handler = open(filename, mode='r')
content = handler.readlines()
for line in content:
	print(line)
handler.close()


filename = 'note_text'
try:
	handler = open(filename, mode='r')
	for line in handler:
		print(line)
except:
	print('something is wrong....')
finally: 
	handler.close()

# IO devices: input/output, error prone
#disk, network, web page, post to web, download from
#because it is not fully controlled by cpu, but by ID devices, 
#easy to get unexepected error compared with regular computing

filename = 'note_text'
# handler = open(filename, mode='r')
with open(filename, mode='r') as handler: 
	# content = handler.read()
	# content = handler.readline()
	for line in handler:
		print(line)

filename = 'my-new-file'
with open(filename, 'w') as handler:
	handler.write('hello, good Wednesday\n')

filename = 'my-new-file'
la = ['Helen ', 'Claire\n', 'Bob\n', '\n']
with open(filename, 'w') as handler:
	handler.writelines(la)

# In computer programming, boilerplate code or 
# just boilerplate are sections of code that are 
# repeated in multiple places with little to no variation. 
# When using languages that are considered verbose, 
# the programmer must write a lot of code to accomplish 
# only minor functionality. Such code is called boilerplate.