#an iterable, like a string, list, or set,
#list, tuple, dictionary are iterable
la = [1, 2, 3]
for x in la:
	print(x)

la = [1, 2, 3, 4, 5, 6, 7, 8]
for x in la: 
	if x % 2 == 0:
		print(f'{item} is even')
 

GPAs = {'Helen': 4.7, 'Clare': 4.7, 'Nancy': 4.3, 'Bob': 3.2}
for x in GPAs:
	print(x)

for x in GPA.keys():
	print(x)

for x in GPA.values():
	print(x)

for x in GPA.items():
	print(x)

for name, score in GPA.items():
	print(f'{name} - {score}')

#range(5)
for i in range(5):
	print(i)


# loop over a string
sa = 'Helen'
for letter in sa:
	print(letter)
	print(f'{letter}-1')

#Multiple Values in a Tuple While Looping
list_of_tuples=[(1,'banana'),(2,'apple'),(3,'pear')]
for number,fruit in list_of_tuples:
	print(f'{number} - {fruit}')

#enumerate returns a tuple in the form of (count, item). 
la = ['Helen', 'Claire', 'Nancy']
for (pos, name) in enumerate(la):
	print(f'{pos} - {name}')


#Nesting Loops
nested=[['mike',12],['jan',15],['alice',8]]
for lst in nested:
	print(f'List = {lst}')
	for item in lst:
		print(f'Item -> {item}')

# Linux command vs Python command

# find a factor of an integer
a = 10
for i in range(1, a+1):
	if a % i == 0: 
		print(i)
# for i in range(1, a):

#error catch
a = input()
a = int(a)
for i in range(1, a+1):
	if a % i == 0: 
		print(i)