from alpha_vantage.timeseries import TimeSeries
from pprint import pprint
import matplotlib.pyplot as plt

key="R6I99SPCSM535HYM"
ts = TimeSeries(key, output_format='pandas')

def export_stk(symbol, fname):
	quote, meta = ts.get_daily_adjusted(symbol=symbol, outputsize='full')
	cols = list(quote.columns[:6])
	del cols[4]
	new_quote = quote[cols]
	new_quote.to_csv(fname)

export_stk('VGT', 'vanguard_stk.csv')






# from alpha_vantage.techindicators import TechIndicators
# from alpha_vantage.sectorperformance import SectorPerformances
# from alpha_vantage.cryptocurrencies import CryptoCurrencies
# from alpha_vantage.foreignexchange import ForeignExchange
# ti = TechIndicators(key, output_format='pandas')
# sp = SectorPerformances(key, output_format='pandas')
# cc = CryptoCurrencies(key, output_format='pandas')
# fex = ForeignExchange(key)
#--------------
# aapl, meta = ts.get_daily_adjusted(symbol='AAPL', outputsize='full')
# google, meta = ts.get_daily_adjusted(symbol='GOOGL', outputsize='full')
# msft, meta = ts.get_daily_adjusted(symbol='MSFT', outputsize='full')