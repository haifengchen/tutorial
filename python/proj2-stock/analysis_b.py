
def read_file(fname):
	with open(fname,'r') as handler:
		lines = handler.readlines()

	title = lines[0]
	fields = title.strip().split(',')
	fields = [col.strip() for col in fields]

	records = []
	for line in lines[1:]:
		values = line.split(',')
		values[1:] = [float(v) for v in values[1:]]
		items = zip(fields, values)
		rec = dict(items)
		records.append(rec)

	return records

def initialize_counters():
	keys = ['1-days', '2-days', '3-days', 
		'4-days', '5-days', '6-days',
		'7-days', '8-days', '9-days', 
		'10-days', '11-days', '12-days', 
		'13-days', '14-days', '15-days' 
		]
	inc_count =dict(zip(keys, [0]*len(keys))) 
	dec_count =dict(zip(keys, [0]*len(keys))) 
	inc_count['longer'] = []
	dec_count['longer'] = []
	return (inc_count, dec_count)


fnames = ['apple_stk.csv',
	  'google_stk.csv',
	  'micorsoft_stk.csv',
	  'vanguard_stk.csv'
	  ]
records = read_file(fnames[0])
L = len(records)
prices = [rec['4. close'] for rec in records]

inc_count, dec_count = initialize_counters()
pos = 0
while pos < L-1:
	i0 = pos
	going_up = True if prices[i0+1] > prices[i0] else False
	cnt = 1
	turn_direction = False
	while not turn_direction:
		i0 = i0+1
		if i0 >= L-1: 
			if going_up:
				pos = i0
				dec_count[f"{cnt}-days"] += 1
				turn_direction = True
			else:
				pos = i0
				inc_count[f"{cnt}-days"] += 1
				turn_direction = True
			break

		if prices[i0+1] > prices[i0] and going_up:
			cnt += 1
		if prices[i0+1] <= prices[i0] and not going_up:
			cnt += 1
		if prices[i0+1] > prices[i0] and not going_up:  #make a turn
			pos = i0
			dec_count[f"{cnt}-days"] += 1
			turn_direction = True
		if prices[i0+1] <= prices[i0] and going_up:  #make a turn
			pos = i0
			inc_count[f"{cnt}-days"] += 1
			turn_direction = True
print(f"stock has {L} or {len(prices)} samples")
print(inc_count)
print(dec_count)
