from enum import Enum

def read_file(fname):
	with open(fname,'r') as handler:
		lines = handler.readlines()

	title = lines[0]
	fields = title.strip().split(',')
	fields = [col.strip() for col in fields]

	records = []
	for line in lines[1:]:
		values = line.split(',')
		values[1:] = [float(v) for v in values[1:]]
		items = zip(fields, values)
		rec = dict(items)
		records.append(rec)

	return records

def initiaize_counters():
	keys = ['1-days', '2-days', '3-days', 
		'4-days', '5-days', '6-days',
		'7-days', '8-days', '9-days', 
		'10-days', '11-days', '12-days', 
		'13-days', '14-days', '15-days' 
		]
	inc_count =dict(zip(keys, [0]*len(keys))) 
	dec_count =dict(zip(keys, [0]*len(keys))) 
	inc_count['longer'] = []
	dec_count['longer'] = []
	return (inc_count, dec_count)

class Status(Enum):
	CONTINUE = 1
	TURN = 2
	TOEND = 3

def check_status(prices, i0):
	if i0 >= L-1:
		return Status.TOEND
	if prices[i0+1] > prices[i0] and going_up:
		return Status.CONTINUE
	if prices[i0+1] <= prices[i0] and not going_up:
		return Status.CONTINUE
	return Status.TURN	

fnames = ['apple_stk.csv',
	  'google_stk.csv',
	  'microsoft_stk.csv',
	  'vanguard_stk.csv'
	  ]
records = read_file(fnames[2])
L = len(records)
inc_count, dec_count = initiaize_counters()
prices = [rec['4. close'] for rec in records]
sm = 0
pos = 0
while pos < L-1:
	i0 = pos
	going_up = True if  prices[i0+1] > prices[i0] else False
	cnt = 1
	status = Status.CONTINUE
	while status is Status.CONTINUE :
		i0= i0+1
		status = check_status(prices, i0)
		if status in  [Status.TOEND, Status.TURN]:
			count = inc_count if going_up else dec_count
			count[f"{cnt}-days"] += 1
			pos = i0
			print(f"{cnt} continuous decrease done..")
			sm += cnt
		if status == Status.CONTINUE :
			cnt += 1

print(f"stock has {L} or {len(prices)} samples")
print(f"total {sm} steps")
print(f"the increase channels: {inc_count}")
print(f"the decrease channels: {dec_count}")
