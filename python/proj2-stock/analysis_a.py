
fnames = ['apple_stk.csv',
	  'google_stk.csv',
	  'micorsoft_stk.csv',
	  'vanguard_stk.csv'
	  ]

def read_file(fname):
	with open(fname,'r') as handler:
		lines = handler.readlines()

	title = lines[0]
	fields = title.strip().split(',')
	fields = [col.strip() for col in fields]

	records = []
	for line in lines[1:]:
		values = line.split(',')
		values[1:] = [float(v) for v in values[1:]]
		items = zip(fields, values)
		rec = dict(items)
		records.append(rec)

	return records

records = read_file(fnames[0])
L = len(records)

keys = ['1-days', '2-days', '3-days', 
	'4-days', '5-days', '6-days',
	'7-days', '8-days', '9-days', 
	'10-days', '11-days', '12-days', 
	'13-days', '14-days', '15-days' 
	]
inc_count =dict(zip(keys, [0]*len(keys))) 
dec_count =dict(zip(keys, [0]*len(keys))) 
inc_count['longer'] = []
dec_count['longer'] = []

prices = [rec['4. close'] for rec in records]
sm = 0
pos = 0
while pos < L-1:
	is_inc, is_dec = False, False
	i0, i1 = pos, pos+1
	if prices[i1] > prices[i0]:
		is_inc = True
	else:
		is_dec = True 
	cnt = 1
	turn_direction = False
	while not turn_direction:
		i0, i1 = i0+1, i1+1
		if i1 >= L: 
			if is_dec:
				pos = i0
				dec_count[f"{cnt}-days"] += 1
				turn_direction = True
				print(f"{cnt} continuous decrease done..")
				sm += cnt
			else:
				pos = i0
				inc_count[f"{cnt}-days"] += 1
				turn_direction = True
				print(f"{cnt} continuous increase done..")
				sm += cnt
			break

		if prices[i1] > prices[i0] and is_inc:
			cnt += 1
		if prices[i1] <= prices[i0] and is_dec:
			cnt += 1
		if prices[i1] > prices[i0] and is_dec:
			pos = i0
			dec_count[f"{cnt}-days"] += 1
			turn_direction = True
			print(f"{cnt} continuous decrease done..")
			sm += cnt
		if prices[i1] <= prices[i0] and is_inc:
			pos = i0
			inc_count[f"{cnt}-days"] += 1
			turn_direction = True
			print(f"{cnt} continuous increase done..")
			sm += cnt
print(f"stock has {L} or {len(prices)} samples")
print(f"total {sm} steps")
print(inc_count)
print(dec_count)
