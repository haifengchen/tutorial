# Install python (version >3.8)
* download from [Python Website](https://www.python.org/downloads/)
* follow instructions to install

# Set up python virtual environment
* locate and create a directory for virtual environment, say: `~/opt`
* `cd ~/opt`
* `python3.9 -m venv ./venv_py39`

# Install VSCODE
* download from [vscode site](https://code.visualstudio.com/)
* follow instructions to install
* launch vscode 

# Install VSCODE plugins
* Python extension for Visual Studio Code
* VSCodeVim

# Create your workspace
* choose a directory for code, say ~/workspace

