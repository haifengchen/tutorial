#  pip install gTTs
# brew install mpg123
import os
os.system('gtts-cli --nocheck " What project that your team did are you most proud of and why?" | mpg123 -q -')
os.system('gtts-cli --nocheck " Where has the team’s technology been used as a result of being part of an industrial lab and why is that special?" | mpg123 -q -')
os.system('gtts-cli --nocheck " Who does your team collaborate with and why is that special?" | mpg123 -q -')
os.system('gtts-cli --nocheck " What success have your prior interns had from this experience? Papers published? Talks given? IP?" | mpg123 -q -')
os.system('gtts-cli --nocheck " How many employees hired full time were prior interns?" | mpg123 -q -')

while True:
	inp = input("What do you want to covert to speech?\n") 
	if inp == "done":
		print(f"You just typed in {inp}; goodbye!")
		os.system(f'gtts-cli --nocheck "You just typed in {inp}; goodbye!" | mpg123 -q -')
		# os.system(f'gtts-cli --nocheck --slow "You just typed in {inp}; goodbye!" | mpg123 -q -')
		break
	else: 
		print(f"You just typed in {inp}")
		os.system(f'gtts-cli --nocheck "{inp}" | mpg123 -q -') 
		continue