# ---- pandas -----------
# https://pandas.pydata.org/docs/getting_started/intro_tutorials/01_table_oriented.html
# 
import pandas as pd
df = pd.DataFrame(
	 {'name': ['Helen', 'Claire', 'Grace', 'Nancy', 'Mike', 'John'],
	  'math': [98, 97, 93, 89, 91, 72],
	  'English': [91, 93, 83, 79, 88, 92], 
	  'PE': [81, 83, 76, 77, 82, 88], 
	 }
	)

scores = pd.read_csv("testscores.csv", header=None)
scores.columns = ['name', 'math', 'english', 'PE']
scores = scores.set_index('name')
scores['math']