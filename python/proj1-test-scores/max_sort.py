#----swap list ---------------
a = 1
b = 10

tmp = a
a = b
b = tmp

a, b = b, a

la = [1, 3, 6, 2, 8, 4]
la[2], la[4] = la[4], la[2]

#-------------------
def find_max_a(la):
	mx = la[0]
	mx_pos = 0
	for i, x in enumerate(la):
		if x > mx:
			mx = x
			mx_pos = i
	return (mx, mx_pos)

def find_max_b(la):
	for i in range(len(la)-1):
		if la[i] > la[i+1]:
			a = la[i]
			la[i] = la[i+1]
			la[i+1] = a
	return la[-1]

mxa = find_max_a([2, 5, 7, 2, 3, 5])
mxb = find_max_b([2, 5, 7, 12, 3, 5])
print(mxa, mxb)

#--------------------------
def find_min_a(la):
	mn = la[0]
	mn_pos = 0
	for i, x in enumerate(la):
		if x < mn:
			mn = x
			mn_pos = i
	return (mn, mn_pos)

def sort_a(la):
	lb = []
	while len(la)>0:
		mn, mn_pos = find_min_a(la)
		lb.append(mn)
		del la[mn_pos]
	return lb
lc = sort_a([2, 5, 3, 7, 12, 1, 8])
print(lc)

#------------ model complexity -----
#N the lendth of list
# N comparison for going over once
# go over N time, N**2 comparisons

# best case 
#worst case

#----------- Selection sort -------------------------
def selection_sort(la):
	for i in range(len(la)):
		min = i
		for j in range(i+1, len(la)):
			if la[j] < la[min]: 
				min = j
		la[min], la[i] = la[i], la[min]
	return la

unsorted = [7, 2, 4, 1, 5, 3]
sorted = selection_sort(unsorted)
print(sorted)

#----------bubble sort ----------------
def bubblesort(L):
    swapped = True
    while swapped:
        swapped = False
        for j in range(len(L) - 1):
            if L[j] > L[j + 1]:
                L[j], L[j + 1] = L[j + 1], L[j]
                swapped = True
    return L

unsorted = [5, 2, 4, 6, 1, 3]
sorted = bubblesort(unsorted)
print(sorted)

# worst case N**2 comparisons
# best case : 1 round O(N)
# https://www.youtube.com/watch?v=uJLwnsLn0_Q&ab_channel=ProgrammingwithMosh
#------------------------------------
def insertionsort(L):
    # loop through all except first element in list
    for index in range(1, len(L)):
        value = L[index]
        position = index - 1
        # inserts the key into the correct place
        while position >= 0 and L[position] > value:
            L[position + 1] = L[position]
            position -= 1
        L[position + 1] = value
    return L

unsorted = [7, 2, 4, 1, 5, 3]
sorted = insertionsort(unsorted)
print(sorted)

# best case : 1 round O(N)
#https://www.youtube.com/watch?v=nKzEJWbkPbQ&ab_channel=ProgrammingwithMosh



