# ---- get the bins -----------
import random

def count_scores(scores, bins):
	counts = {}
	for bin in bins:
		counts[bin] = 0
	for score in scores:
		for bin in bins:
			if score>bin[0] and score <=bin[1]:
				counts[bin] += 1
	return counts

bins = [(0, 60), (60, 70), (70, 80), (80, 90), (90, 100)]
math = [random.randrange(50, 101) for i in range(1000)]
pe = [random.randrange(50, 101) for i in range(1000)]
math_cnt = count_scores(math, bins)
pe_cnt = count_scores(pe, bins)

#--------------------------------
import matplotlib.pyplot as plt
import numpy as np

def plot_together():
	x = np.arange(len(pe_cnt))  # the label locations
	width = 0.35  # the width of the bars
	fig, ax = plt.subplots()
	rects1 = ax.bar(x - width/2, math_cnt.values(), width, label='Math')
	rects2 = ax.bar(x + width/2, pe_cnt.values(), width, label='PE')

	ax.set_ylabel('Scores')
	ax.set_title('Scores of Math and PE')
	ax.set_xticks(x)
	ax.set_xticklabels(pe_cnt.keys())
	ax.legend()

	ax.bar_label(rects1, padding=3)
	ax.bar_label(rects2, padding=3)

	fig.tight_layout()
	plt.show()

#--------------------------------
def plot_separately():
	x = np.arange(len(pe_cnt))  # the label locations
	width = 0.35  # the width of the bars
	fig, (ax1, ax2) = plt.subplots(1, 2)

	def plot_ax(ax, cnt, title):
		rects = ax.bar(x, cnt.values(), width, label='Math')
		ax.set_ylabel('Scores')
		ax.set_title(title)
		ax.set_xticks(x)
		ax.set_xticklabels(cnt.keys())
		ax.legend()
		ax.bar_label(rects, padding=3)

	plot_ax(ax1, math_cnt, 'Scores of Math')
	plot_ax(ax2, pe_cnt, 'Scores of PE')

	fig.tight_layout()
	plt.show()

#--------------------------------
def pie_plot():
	explode = (0, 0.1, 0, 0, 0)  # only "explode" the 2nd slice (i.e. 'Hogs')
	fig1, (ax1, ax2) = plt.subplots(2, 1)
	ax1.pie(math_cnt.values(), explode=explode, labels=math_cnt.keys(), autopct='%1.1f%%', shadow=True, startangle=90)
	ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
	ax1.set_title('Math score')
	ax2.pie(pe_cnt.values(), explode=explode, labels=pe_cnt.keys(), autopct='%1.1f%%', shadow=True, startangle=90)
	ax2.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
	ax2.set_title('PE score')
	plt.show()


#--------------------------------
def scatter_plot():
	fig, ax = plt.subplots()
	ax.scatter(math, pe, marker='*')
	plt.show()

scatter_plot()