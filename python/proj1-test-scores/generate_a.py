import names
from random import randrange
fname = 'testscores.csv'
lines = ''
for i in range(4):
	la = [None]*4
	la[0] = names.get_full_name()
	la[1] = str(randrange(50, 101))
	la[2] = str(randrange(50, 101))
	la[3] = str(randrange(50, 101))
	line = ','.join(la)
	lines = lines+line+'\n' 

with open(fname, 'w') as handler:
	handler.write(lines)
