tup1 = ('A', 'B', 'C')
dict1 = {'A': 1, 'B':2}

s1 = {'A', 'B', 'C'}
s2 = {'A', 'B', 'C', 'B', 'A'}
print(type(s1))
print(s1)
print(s2) #dupliacted removed

la = ['Helen', 'Claire', 'Bob', 'Claire', 'Mike']
sa = set(la)

#Sets do not allow you to use slicing or the like to access individual members of the set. 
# Instead, you need to iterate over a set. 
# You can do that using a loop, such as a while loop or a for loop.
len(s1), len(s2)
#sa[1]
for name in sa:
	print(name)

#for i in range(len(sa)-1):
#	print(sa[i])

#start from empty, then add
s4 = () #not much useful
s4 = [] 
s4.append(2)
s3 = {} #dictionary
s3['name'] = 'Helen'

s1= list()
s2 = tuple()
s2 = dict()
s3 = set()

sa.add('Grace')
sa.add('Helen')
#add multiple at once
sa.update(['Kevin', 'Grace', 'Alison'])

sa.remove('Mike')

sa.remove('Mike') #err, not exist
sa.discard('Mike') #no err

sa.clear() 
print(sa)
del sa

#-----------------
band = {'Helen', 'Claire', 'Alison', 'Kevin', 'Bob'}
cross_country = {'Kian', 'Kevin', 'Alison', 'Grace', 'William'}

all_kids = band.union(cross_country)
attend_both = band.intersection(cross_country)
inband_not_run = band.difference(cross_country)
inrun_notin_band = cross_country.difference(band)

#-------------------------------------------------------------------
abs(-0.2)
pow(3, 3)
pow(4, 0.5)
sum([2,4, 5])
5%2  #modulo, remainder
# used for check, if x %2 ==0: even
divmod(5,2)

#sqrt(4), floor/ceil/trunc, exp/log, sin/cos, factorial, gcd/lcm, lcd

#[0.01] *10 ==?

import math
print(math.pi)
print(math.e)
print(math.inf)
#find max, initial mx = -math.inf
#find min, initial mn = math.inf
print(math.nan)
#not a number

math.sqrt(4)
f = 3.96
a = math.floor(f)
b = math.ceil(f)
c = math.trunc(f)
d = int(f)
a, b, c, d

#4!
math.factorial(4)

math.gcd(8,6)
#math.lcm(8,6) in py3.9

math.pow(3,4)
math.log(8,2) #2 is the base
math.log(2,8)
math.log(1000,10)
math.log(10) #base e
math.log10(1000)
math.log2(8)

#---- angles ----
ang_rad1 = math.pi/6
ang_deg1 = math.degrees(ang_rad1) #pay attention, not exactly 3, numerical 
ang_deg1 == 30
math.isclose(ang_deg1, 30, rel_tol=0.00001)

ang_deg2 = 75
ang_rad2 = math.radians(ang_deg2)

# only accept radians
math.sin(ang_rad1)
math.sin(math.pi/2)
math.cos(math.pi/2) # not exactly 1

math.sin(math.pi/6)
math.cos(math.pi/6)

math.sin(math.pi/2)
math.cos(math.pi/2)
















