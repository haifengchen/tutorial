
# how much about time? million sec, micro second?
# ---- time ----
import time

#the number of seconds since 0:00 on January 1, 1970. unix invented
t1 = time.time()
print(t1)

#return the string of current time, but not frequently used
s1 = time.ctime() #string
time.sleep(5) #sleep 5 seconds

# used for check how long the program runs
from random import randrange
t0 = time.time()
for i in range(1000000):
	a = randrange(0, 100)
	b = randrange(0, 100)
	c = a + b
t1 = time.time()
duration_in_seconds = t1 - t0

#introduce time zone
# https://en.wikipedia.org/wiki/Time_zone
# how many time zones in world? in USA? what is UTC? we are in what time zone?
#-------------------------------------
import datetime
t1 =  datetime.time(1, 2, 3)
print(type(t1)) . #not basic type, object with 'time' class
# concept of class, compare with function. wheel/engine vs a car, many functions make an object
print(t1)

#A time instance holds only values of time; it does not include a date associated with the time.
tmin = datetime.time.min # a constant value, not a method
tmax = datetime.time.max 
resolution = datetime.time.resolution #micro seconds
print(tmin, tmax, resolution)

lunch = datetime.time(10, 45, 0, microsecond=0)
lunch.hour
lunch.minute
lunch.second
lunch.microsecond

#-------------------------------------
today = datetime.date.today()
school_start = datetime.date(2021, 9, 7)
# both today and schoo-_start is a object, not basic type
school_start.year
school_start.month
school_start.day
school_start.weekday()
# it is a methid, not a constant variable!!
# 5 means Saturday, Monday is 0 and Sunday is 6.

#the Gregorian calendar, where January 1 of the year 1 is 
# designated as having the value 1 and each subsequent day increments the value by 1.
o = 733114
d1 = datetime.date.fromordinal(733114)
d1.year, d1.month, d1.day

days1 = today.toordinal() #integer
helen_birth = datetime.date(2008, 10, 24)
days2 = helen_birth.toordinal()
days_till_now = days1 - days2

#-------------------------------------
#basic arithmetic on two datetime objects, or by combining a datetime with a timedelta. 
# Subtracting dates produces a timedelta, and a timedelta can be also added or subtracted 
# from a date to produce another date. 
# The internal values for a timedelta are stored in days, seconds, and microseconds.

duration = datetime.timedelta(days=2, hours=5, minutes=23, seconds=55, milliseconds=22, microseconds=223)
print(type(duration))
print(duration)
duration.total_seconds() #method

lunch_period = datetime.timedelta(minutes=30, seconds=30)
band_period = datetime.timedelta(hours=1, minutes=15)
#support math operation
ratio = lunch_period/band_period

workout_session = datetime.timedelta(hours=2, minutes=15) 
warmup_session = datetime.timedelta(minutes=25, seconds=35) 
swimming_session = workout_session - warmup_session
print(swimming_session)
print(swimming_session.total_seconds())

now = datetime.date.today()
project_length = datetime.timedelta(days = 10)
due_day = now + project_length
print(due_day)
due_day.month, due_day.day, due_day.weekday()

#---------------------------------
#comparing the values
helen_birthday = datetime.date(2008, 10, 24)
beijing_olympics = datetime.date(2008, 8, 8)
print(f'is helen born after Olympics? : {helen_birthday > beijing_olympics}')

#---------------------------------
t1 = datetime.time(8, 23, 45)
d1 = datetime.date(2021, 3, 18)
dt1 = datetime.datetime.combine(d1, t1)

dt2 = datetime.datetime.now()
dt3 = datetime.datetime.today()  #different with brfore

birthtime = datetime.datetime(2008, 10, 24, 1, 23, 55, 5556)
birthtime.year, birthtime.month, birthtime.day, birthtime.minute, 
#---------------------------------

# format for parsing and print
# datetime object, <---> string
# read: 10/7/2001, 10-7-2001, Oct 7 2001, October 7 2001 ??
# print: datetime object --> which format to print?

format = "%a %b %d %H:%M:%S %Y"
birthtime = datetime.datetime(2008, 10, 24, 1, 23, 55, 5556)
s1 = birthtime.strftime(format)

la = ['10-2-2008', '1-3-2018', '7-3-2012', '5-15-2014'] 
lb = []
fmt = '%m-%d-%Y'
for d in la:
	dt = datetime.datetime.strptime(d, fmt)
	lb.append(dt)

s2 = '8/13/2016'
fmt2 = '%m/%d/%Y'
dt = datetime.datetime.strptime(s2, fmt2)
# once datetime object, comparison, add delta, get the year/date...

s3 = '13:26:37 9-3-2021'
fmt3 = "%H:%M:%S %m-%d-%Y" 
dt = datetime.datetime.strptime(s3, fmt3)

#https://www.nbshare.io/notebook/510557327/Strftime-and-Strptime-In-Python/

# --- calendar ------
import calendar

c = calendar.TextCalendar(calendar.SUNDAY)
c.prmonth(2021, 9)
c.pryear(2021)

hc = calendar.HTMLCalendar(calendar.SUNDAY)
print(hc.formatmonth(2016, 5))