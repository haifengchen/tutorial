
la = ['a', 'b', 'c'] #list
ta = ('a', 'b', 'c') #tuple
da = {'a': 1, 'b':3, 'c':7} #dictionary

# Set
s1 = {'a', 'b', 'c'}

la2 = ['a', 'a', 'b', 'b']
s2 = set(la2)
print(s2)
print(type(s2))

la = ['Claire', 'Helen', 'Alison', 'Alison', 'Mike', 'John']
sa = set(la)
print(len(la), len(sa))

la[1]
#sa[1]

for kid in sa:
	print(kid)

la = []
la = list()
la.append('H')

da = {}
da = dict()
da['name'] = 'Helen'

ta = () #

sa = set()
sa.add('Green')
sa.add('Red')
print(sa)
sa.remove('Green')

sa.clear()
del sa

#-----------------------------

band = ['Helen', 'Claire', 'Alison', 'Kevin', 'Mike']
cross_country = ['Alison', 'Bob', 'Nancy', 'Kevin']

band = set(band)
cross_country = set(cross_country)
all = band.union(cross_country)
print(all)

attend_both = band.intersection(cross_country)
print(attend_both)

only_band_not_run = band.difference(cross_country)
print(only_band_not_run)

only_run_not_band = cross_country.difference(band)
print(only_run_not_band)

#----------------------------------------
abs(-16)
pow(2,3)
sum([3, 4, 5, 6])
5 % 2
x = 7
if x % 2 ==0 :
	print('x is even')
divmod(5, 2)

#5!, log, gcd, lcm
import math
math.pi
math.e
math.inf
-math.inf
initial = -math.inf

math.factorial(10)
math.gcd(8,6)
math.lcm(8,6)

math.log(8, 2)
math.log(100, 10)
math.log(10)
math.log2(8)
math.log10(100)

#pi/3 radian degree
math.radians(30)

#-----------------------------
import random

#0, 1
v1 = random.random()
print(v1)

# 10, 20
l = 10
h = 20
v1 = random.random()
v2 = (h-l)*v1 + l
print(v2)

i1 = random.randint(0, 100)
print(i1)

i2 = random.randrange(0, 100, 5)
print(i2)

#seed
random.seed(178)
teams = ['T', 'G', 'M', 'S']
student_a = {'name': 'Helen Chen',
	     'team': random.choice(teams)}
print(student_a)

present_order = ['Claire', 'Helen', 'Nancy', 'Bob']
random.shuffle(present_order)
print(present_order)

students = ['Claire', 'Helen', 'Nancy', 'Bob']
selected = random.sample(students, 2)
print(selected)

#--------------------------------
names = ['Helen', 'Claire', 'Nancy', 'Kevin']
scores = [95, 97, 89, 91]
#[('Helen', 95), ('Claire', 97), }
#{'Helen':95, 'Claire':97, ...}
za = zip(names, scores)
list(za)
dt1 = dict(zip(names, scores))




