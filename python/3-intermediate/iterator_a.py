# Iterable--> iterator,  generator

#This is usually done using a for-loop. Objects like lists, tuples, sets, dictionaries, strings, etc.
#  are called iterables. In short, anything you can loop over is an iterable.

#what type of obj: list, tuple, dictionary, set, string, file.
for x in obj:
	pass

# An iterator is an object representing a stream of data i.e. iterable. 
# make the iterable to iterate
# They implement something known as the Iterator protocol 
#example: frog is swimmable (but there are more other attr, runnable, jumpable, talkable), in-water (iterator)

la = ['A', 'B', 'C', 'D']

it = iter(la)
s1 = next(it)
s2 = next(it)
s3 = next(it)
s4 = next(it)
s5 = next(it) # error, but we don't know the len at the beginning, aftr try

it = iter(la)
while True:
    # this will execute till an error is raised
    try:
        val = next(it)
    # when we reach end of the list, error is raised and we break out of the loop
    except StopIteration:
        break
    print(val)

#If you take a step back, you will realize that this is precisely 
# how the for-loop works under the hood.
#  What we did with the loop we made here manually,
#  for-loop does the same thing automatically. 
# And that is why for-loops are preferred for looping over the iterables 
# because they automatically deal with the exception.
for val in la: 
	print(val)

# why to learn that? 1) understand underlying; 2) build own iterable; 3) generator
#iterator comes from iterable; can we build next() without iterable (list, dict,...)
#generator
# Generators are also iterators but dones't need to come from Iterable
# benifit: memory, size, on-demand

#generate number 1, 2, 3, ...100 #data type?? list 
def generate_numbers(N):
	la = []
	for i in range(N):
		la.append(i+1)
	return la
lnumbers = generate_numbers(10)

#get a bunch, in memory, 
# can we generate one by one, on demand
def num_generator(N):
	for i in range(N):
		yield i
gen = num_generator(10)  #put into water
gen, type(gen)
next(gen)
next(gen)
next(gen)

#NOTE where it is started
for i in gen:
	print(i)

#iterator, go through to the end, need to reset if need to restart 
gen = num_generator(10)  #put into water from the beginning
for i in gen:
	print(i)

#good 1: list all together, need to remember index,   next() provide 
la = generate_numbers(1000000)
ga = num_generator(1000000)  #put into water
import sys
print('Size of list in memory',sys.getsizeof(la))
print('Size of generator in memory',sys.getsizeof(ga))
# both can be used in the "for" loop
for i in la:
	pass
for i in ga:
	pass
 
#------------------------------
#good 2: infinite sequences, or you don't know when to end
# list can't do

def fininte_squares(N):
	i = 1
	while i < N+1:
		yield i**2

ga = fininte_squares(10)
next(ga)

ga = fininte_squares(10)
for x in ga:
	print(x)

def infinite_squares():
	i = 1
	while True:
		yield i**2
ga = infinite_squares()
ga.next() #as many you want


ga = fininte_squares(10)
for x in ga:  #infinte loop
	print(x)
	if x > 2000:  #you don't know how many at the beginning
		break

#Fibnacci: Fn = Fn-1 + Fn-2
def fib():
	F_1, F_2 = 1, 0
	while True:
		F = F_1 + F_2
		yield F
		F_2 = F_1
		F_1 = F

gf = fib()
next(gf)
next(gf)
next(gf)
next(gf)

gf = fib()
for x in gf:
	print(x)
	if x >= 2000:
		break
		
#---------------------------
#inifnaite can only be in 'next()', for loop will forwver 
#nex(g) can be any places
#-------------------------------------

# another way to make generator, not using function, 

la = [ x**2 for x in range(10)]
ga = ( x**2 for x in range(10)) 
la[0]
next(ga) #no call func
for i in ga:
	print(i)

import sys
# list comprehension
mylist = [i for i in range(10000000)]
print('Size of list in memory',sys.getsizeof(mylist))
# generator expression
mygen = (i for i in range(10000000))
print('Size of generator in memory',sys.getsizeof(mygen))

x = [x**2 for x in range(100) if x%2 ==0]
w = ((x, y) for x in range(100) for y in range(100) if x**2 + y**2 <100)


def integers():
    i = 1
    while True:
        yield i
        i = i + 1

pyt = ((x, y, z) for z in integers() 
		for y in range(1, z) 
		 for x in range(1, y) if x*x + y*y == z*z)

#-------- read file -----------

with open(fname, 'r') as handler:
	lines = handler.readlines()
	for line in lines:
		print(line)
	

with open(fname, 'r') as handler:
	for line in handler: #handler is an iterable
		print(line)  

fname = 'google_stk.csv'
with open(fname, 'r') as handler:
	lines = (line for line in handler)
	header = next(lines)
	for line in lines:
		print(line)
print(header)


fname = 'google_stk.csv'
with open(fname, 'r') as handler:
	lines = (line for line in handler)
	header = next(lines)
	fields = header.strip().split(',')
	fields = [cols.strip() for col in fields]

	records = []
	for line in lines:
		values = line.strip().split(',')
		values[1:] = [ float(v) for v in values[1:]]
		items = zip(fields, values)
		rec = dict(items)
		records.append(rec)
#-------------------------------