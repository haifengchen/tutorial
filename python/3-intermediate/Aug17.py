
for x in 'Helen':
	print(x)

#list, tuple, file, set, dictionary, string 
# "Iterable" ---> "Iterator"
la = ['a', 'b', 'c']
it = iter(la)
next(it)
next(it)
next(it)
next(it) #error

for x in la:
	pass

it = iter(la)
while True:
	try: 
		v1 = next(it)
	except:
		break
	print(v1)

#frog , "swimmable",  put into water, 

#---------------------
#1, 2,3, ...100
def seq_generator_a():
	la = []
	v = 0
	while v < 100:
		v = v +1
		la.append(v)
	return la

def seq_generator_b():
	# la = [v+1 for v in range(100)]
	la = []
	for v in range(100):
		la.append(v+1)
	return la

la = seq_generator_a()
lb = seq_generator_b()

def seq_generator_c():
	for v in range(100):
		yield v

ga = seq_generator_c()
next(ga)
next(ga)
next(ga)
next(ga)

for x in ga:
	print(x)

ga = seq_generator_c()
for x in ga:
	print(x)
#-----------------

def seq_generator_d(N):
	# la = [v+1 for v in range(100)]
	la = []
	for v in range(N):
		la.append(v+1)
	return la


def seq_generator_e(N):
	for v in range(N):
		yield v

ld = seq_generator_d(1000000)
le = seq_generator_e(1000000)
import sys
sys.getsizeof(ld)
sys.getsizeof(le)

def seq_generator_f():
	i = 0
	while True:
		i += 1
		yield i

gb = seq_generator_f()
next(gb)
#------------ 



