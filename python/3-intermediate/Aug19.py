import time

t0 = time.time()
print(t1)

#seconds since 0:0 Jan 1 1970
#0.6927865s  =  692.786 milliseconds
#0.6927865s  =  692786.5 microseconds

t1 = time.time()
la = []
for x in range(1000000):
	la.append(x)
t2 = time.time()
duration = t2 - t1

time.sleep(5)

#-------------------
import datetime

t1 = datetime.time(12, 37, 0)
print(t1)
t1.hour
t1.minute
t1.second

print(datetime.time.min)
print(datetime.time.max)
print(datetime.time.resolution)

lunch = datetime.time(10, 40, 0)
print(lunch)
lunch.minute

#----------------------------
d1 = datetime.date(2021, 8, 19)
print(d1)

d2 = datetime.date.today()
print(d2)

school_start = datetime.date(2021, 9, 7)
school_start.month
school_start.day
school_start.weekday()
#Mon:0, Tues:1, ... Sun:6

today = datetime.date.today()
d3 = today.toordinal()
print(d3)
# 2021-8-19
# 0001-1-1

o = 730014
d1 = datetime.date.fromordinal(o)
print(d1)

helen_birthday = datetime.date(2008, 10, 24)
d3 = helen_birthday.toordinal()

claire_birthday = datetime.date(2007, 10, 4)
d4 = claire_birthday.toordinal()
days = d3 -d4
 #----------------------

duration = datetime.timedelta(days=2, hours=5, minutes=3, seconds=45)
print(today)
later_day = today + duration

d5 = datetime.datetime(2021, 8, 19, 12, 37, 56)
print(d5)

d6 = datetime.datetime.now()
print(d6)