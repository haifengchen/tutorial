import random

#uniform between 0, 1
v1 = random.random()

start = 10
end = 20
v2 = random.random() * (end-start) + start

v3 = random.randint(1, 100)
v4 = random.randrange(0, 101, 5) #multiple of 5

random.seed(12)
random.randint(1, 100)
#randomly generate the score for every student, but want to be the same in other cases

#random pick
teams = ['H', 'M', 'N', 'K', 'P']
student_a = {'name': 'Helen Chen', 
	   'team': random.choice(teams)}
print(student_a)

#shuffle the order 
present_order = ['Claire', 'Helen', 'Grace', 'Mike', 'Kevin', 'Nancy']
random.shuffle(present_order)
print(present_order)

group = ['Claire', 'Helen', 'Grace', 'Mike', 'Kevin', 'Nancy']
selected_one = random.choice(group)
selected_many = random.sample(group, 3)
print(selected_one)
print(selected_many)
# program do a lot of simulation, COVID/market/education, if/then, random is needed

#----- ZIP ----- 
names = ['Claire', 'Helen', 'Nancy', 'Kevin', 'Mike', 'Alison']
scores = [97, 95, 89, 96, 88, 91]
items = zip(names, scores)
#list(items)
record = dict(items)
print(record)


#---------- 
import numpy as np
import matplotlib.pyplot as plt

mean = [75, 75]
cov = [[15, 10], [10, 15]] 
x, y = np.random.multivariate_normal(mean, cov, 500).T
plt.plot(x, y, 'x')
plt.axis('equal')
plt.show()

import random
N = random.randint(1, 1000)
print(f"the seed is {N}")
np.random.seed(N)
mean = [75, 80, 85]
cov = [[25, 15, 6], [15, 20, 6], [6, 6, 10]] 
math, eng, pe = np.random.multivariate_normal(mean, cov, 500).T
# plt.plot(math, eng, 'x')
plt.plot(math, pe, 'x')
plt.axis('equal')
plt.show()

