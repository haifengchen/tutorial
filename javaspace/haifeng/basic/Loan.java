package javaspace.haifeng.basic;
import java.util.Scanner;

public class Loan {
    double yearlyIntRate;

    public Loan(double yearlyIntRate){
        this.yearlyIntRate = yearlyIntRate;
    }

    public Loan(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the yearly interests rate: ");
        yearlyIntRate = scanner.nextDouble();
        // this.yearlyIntRate = yearlyIntRate; NO Need!!!
    }

    void printLoan(){
        System.out.print("your input is : ");
        System.out.println(yearlyIntRate);
    }

    public static void main(String[] args) {
        Loan firstLoan = new Loan(0.34);
        firstLoan.printLoan();

        Loan secondLoan = new Loan();
        secondLoan.printLoan();
    }
    
}
