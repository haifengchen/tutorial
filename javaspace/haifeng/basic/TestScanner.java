package javaspace.haifeng.basic;

import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class TestScanner {

    void testA(){
        Scanner sin = new Scanner(System.in);
        System.out.println("pls input a double:");
        double a = sin.nextDouble();
        System.out.println("pls input a int:");
        int b = sin.nextInt();
        System.out.println("pls input a float:");
        float c = sin.nextFloat();
        System.out.println("pls input a line:");
        String line = sin.nextLine();
        System.out.println(a + " :: " + 
                            b + " :: " + 
                            c + " :: " + 
                            line + " :: ");

    }

    void testB(){
        String nums = "10.1 99.88 12.1 3.6 9.2";
        Scanner sin = new Scanner(nums);
        float sum = 0;
        int count = 0;
        while(sin.hasNextFloat()) { 
            sum += sin.nextFloat(); 
            count++;
         }
        System.out.println("the sum is : " + sum);
        System.out.println("count of numbers: " + count);
    }

    void testC(){
        String nums = "10 99.88 2 3.6 9.2";
        Scanner sin = new Scanner(nums);
        float sumFloat = 0;
        int sumInt = 0;
        int countFloat = 0, countInt = 0;
        while(sin.hasNext()) { 
            if (sin.hasNextInt()){ //Int must be first!!!
                sumInt += sin.nextFloat(); 
                countInt++;
            }
            else {
                sumFloat += sin.nextFloat(); 
                countFloat++;
            }
         }
        System.out.println("the sum of float is : " + sumFloat);
        System.out.println("count of float: " + countFloat);
        System.out.println("the sum of int is : " + sumInt);
        System.out.println("count of int: " + countInt);
    }

    void testD() {
        try (FileReader fin = new FileReader("data/testa.txt")) {
            Scanner sin = new Scanner(fin); 
            String line;

            while (sin.hasNextLine()){
                line = sin.nextLine();
                System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        TestScanner tester = new TestScanner();
        // tester.testA();
        // tester.testB();
        // tester.testC();
        tester.testD();
    }

}
